package com.app.forecast.controller;

import com.app.forecast.client.DatabaseMeasurementClient;
import com.app.forecast.model.Forecast;
import com.app.forecast.model.Measure;
import com.app.forecast.service.ForecastService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
@PreAuthorize("hasRole('USER')")
public class MainController {

    @Autowired
    private DatabaseMeasurementClient databaseMeasurementClient;

    @Autowired
    private ForecastService forecastService;

    @RequestMapping("/")
    public Principal resource(Principal principal) {
        return principal;
    }

    @RequestMapping(value = "/current", method = RequestMethod.GET)
    public Measure currentMeasure() {
        return databaseMeasurementClient.getMeasure();
    }

    @RequestMapping(value = "/forecast", method = RequestMethod.GET)
    public Forecast currentForecast() {
        return forecastService.getForecast();
    }

    @RequestMapping(value = "/list10", method = RequestMethod.GET)
    public List<Measure> listMeasure() {
        return databaseMeasurementClient.getMeasures(10);
    }

}
