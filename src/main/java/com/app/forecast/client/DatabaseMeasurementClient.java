package com.app.forecast.client;

import com.app.forecast.model.Measure;

import java.util.List;

public interface DatabaseMeasurementClient {

    Measure getMeasure();

    List<Measure> getMeasures(int countMeasures);
}
