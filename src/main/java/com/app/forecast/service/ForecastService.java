package com.app.forecast.service;

import com.app.forecast.model.Forecast;

public interface ForecastService {

    Forecast getForecast();
}
