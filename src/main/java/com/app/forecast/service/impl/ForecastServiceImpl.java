package com.app.forecast.service.impl;

import com.app.forecast.client.DatabaseMeasurementClient;
import com.app.forecast.model.Forecast;
import com.app.forecast.model.Measure;
import com.app.forecast.service.ForecastService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;

public class ForecastServiceImpl implements ForecastService {

    private @Value("${forecast.measure.size}") Integer measureSize;

    @Autowired
    DatabaseMeasurementClient databaseMeasurementClient;

    @Override
    public Forecast getForecast() {
        Forecast forecast = new Forecast();

        int temperature = 0;
        int humidity = 0;
        int pressure = 0;
        int windSpeed = 0;

        List<Measure> measures = databaseMeasurementClient.getMeasures(measureSize);

        for (Measure measure : measures) {
            temperature += measure.getTemperature();
            humidity += measure.getHumidity();
            pressure += measure.getPressure();
            windSpeed += measure.getWindSpeed();
        }

        forecast.setTemperature(temperature / measures.size());
        forecast.setHumidity(humidity / measures.size());
        forecast.setPressure(pressure / measures.size());
        forecast.setWindSpeed(windSpeed / measures.size());

        return forecast;
    }
}
