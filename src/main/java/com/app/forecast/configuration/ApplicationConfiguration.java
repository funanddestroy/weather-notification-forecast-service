package com.app.forecast.configuration;

import com.app.forecast.client.DatabaseMeasurementClient;
import com.app.forecast.client.impl.InfluxDatabaseMeasurementClient;
import com.app.forecast.service.ForecastService;
import com.app.forecast.service.impl.ForecastServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

    @Bean
    public DatabaseMeasurementClient databaseMeasurementClient() {
        return new InfluxDatabaseMeasurementClient();
    }

    @Bean
    public ForecastService forecastService() {
        return new ForecastServiceImpl();
    }
}
